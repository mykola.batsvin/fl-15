const $list = $(".list");
const $input = $("#add-input");
const $add = $("#add-submit");


const todos = [
  {
    text: "Buy milk",
    done: false
  },
  {
    text: "Play with dog",
    done: true
  }
];

/*$add.click(function () {
  $list.append($('<li>', {
    text: $input.val()
  }));
});*/

/*$(document).ready(function () {
  $add.click(function () {
    // If something is written
    if ($input.val().length != 0) {
      //Store previous data
      var x = $list.html();

      // Add typed text in alert container
      var y = `<li>` + $input.val() + `</li>`;

      //Update
      $list.html(y + x);
      //Clear after addition
      $input.val("");
    } else alert("Enter some Text!");
  });
  $(document).on('click', '.alert', function () {
    if ($(this).css('text-decoration-line') == "none")
      $(this).css('text-decoration-line', 'line-through');
    else
      $(this).css('text-decoration-line', 'none');
  });
});*/

$(function () {
  $add.on("click", function () {
    if (!$input.val()) {
      return false;
    }
    $list.append("<li class='item'><span class='item-text'>"
      + $input.val() + "</span> <button class = 'item-remove'>Remove</button></li>");
    $input.val("");
    $(".item-remove").on("click", function () {
      var $parent = $(this).parent();
      $parent.remuve();
    })
  })
})
