// Your code goes her
/*function getUsers(username) {
   return new Promise((resolve) => {
      const req = new XMLHttpRequest();
      req.open("GET", "https://jsonplaceholder.typicode.com/" + username);

      req.onload = (response) => {
         resolve(response);
      }
      req.send()
   });
}*/

fetch('https://jsonplaceholder.typicode.com/users')
   .then((response) => response.json())
   .then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts', {
   method: 'POST',
   body: JSON.stringify({
      title: 'foo',
      body: 'bar',
      userId: 1
   }),
   headers: {
      'Content-type': 'application/json; charset=UTF-8'
   }
})
   .then((response) => response.json())
   .then((json) => console.log(json));
fetch('https://jsonplaceholder.typicode.com/posts/1', {
   method: 'PATCH',
   body: JSON.stringify({
      title: 'foo'
   }),
   headers: {
      'Content-type': 'application/json; charset=UTF-8'
   }
})
   .then((response) => response.json())
   .then((json) => console.log(json));
fetch('https://jsonplaceholder.typicode.com/posts/1', {
   method: 'PUT',
   body: JSON.stringify({
      id: 1,
      title: 'foo',
      body: 'bar',
      userId: 1
   }),
   headers: {
      'Content-type': 'application/json; charset=UTF-8'
   }
})
   .then((response) => response.json())
   .then((json) => console.log(json));
fetch('https://jsonplaceholder.typicode.com/posts/1', {
   method: 'DELETE'
});