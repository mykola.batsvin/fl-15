// Your code goes here
const array = [1, 1, 2, 3, 4, 56, 7, 8, 76, 5, 241, 5, 356, 567, 2]
//1
function maxElement(array) {
   return Math.max.apply(null, array);
}
console.log(maxElement(array))
//2
function copiedArray(array) {
   const cloneArray = [...array];
   return cloneArray
}
console.log(copiedArray(array))


//3

function addUnicueId(element) {
   let v = { 'unique identifier': Symbol() }
   let copy = Object.assign({}, element, v)
   return copy
}
console.log(addUnicueId({ name: 123 }))


//4
let arr = [{ name: "Location 1", details: { id: 7, age: 11, university: "UNI" } }]
let obj = arr.reduce((a, c) => {
   if (a[c.details.id]) {
      a[c.details.id].push(c)
   } else {
      a[c.details.university] = [c]
   }
   return a
}, {})
console.log(obj)


//5
function findsUniqueElements(array) { //finds Unique Elements
   let unique = [...new Set(array)]
   return unique
}
console.log(findsUniqueElements(array))
//6
const phoneNumber = "0123456789"
function hideNumber(phone) { //hide Number
   const last4Digits = phone.slice(-4);
   const maskedNumber = last4Digits.padStart(phoneNumber.length, '*');
   return maskedNumber;
}
console.log(hideNumber(phoneNumber))
//7
function required() {
   throw new Error('Missing property');
}
function add(aRequiredParameter = required(), aRequiredParameter2 = required()) {

   let c = aRequiredParameter + aRequiredParameter2
   console.log(c);
}
add(1, 1)
//8
function getUser() {
   let arrayOfNames = []
   fetch('https://jsonplaceholder.typicode.com/users')
      .then((response) => response.json())
      .then((json) => json.sort(function (a, b) {
         if (a['name'] >= b['name']) {
            return 1
         } else {
            return -1
         }
      }).forEach((elem) => {
         arrayOfNames.push(elem['name'])
      }));
   console.log(arrayOfNames)

}
getUser()

//9
