// Your code goes here

function shuffle(array) {
   array.sort(() => Math.random() - 0.5);
}



class Card {
   constructor(suit, rank, isFaceCard) {
      this.suit = suit;
      this.rank = rank;
      this.isFaceCard = isFaceCard;
   }
   set isFaceCard(isFaceCard) {
      this.isFaceCard = isFaceCard;
   }

   toString() {
      let value = ['Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King'];
      console.log(value[this.rang] + ' of ' + this.suit)
   }
   static compare(cardOne, cardTwo) {
      return cardOne.rang - cardTwo.rang
   }
}

class Deck {
   constructor(cards, count) {
      this.cards = cards;
      this.count = count;
   }

   set count(count) {
      this.count = count;
   }
   draw(n) {
      let result = [];
      while (n) {
         result.append(this.cards[-1]);
         delete this.cards[-1];
         n--;
      }
      return result;
   }
   shuffle() {
      this.cards = shuffle(this.cards);
   }
}

class Player {


   constructor(name, wins, deck) {
      this.name = name;
      this.wins = wins;
      this.deck = deck;
      wins = 0;
   }
   static Play(playerOne, playerTwo) {
      while (this.deck.length) {
         let round = this.deck.draw(2)
         if (Card.compare(round[0], round[1]) > 0) {
            playerOne.wins++
         } else {
            playerTwo.wins++
         }
      }
      Deck.shuffle
      if (playerOne.wins > playerTwo.wins) {
         console.log(playerOne.name + ' wins ' + playerOne.wins + 'to' + playerTwo.wins)
      } else {
         console.log(playerTwo.name + ' wins ' + playerTwo.wins + 'to' + playerOne.wins)
      }
   }
}





/* class Employee {
   constructor(id, firstName, lastName, birthday, salary, department) {
      this.id = id;
      this.firstName = firstName;
      this.lastName = lastName;
      this.birthday = birthday;
      this.salary = salary;
      this.department = department;
      this.age = age;
      this.fullName = fullName;
      this.EMPLOYEES = EMPLOYEES;

   }
   get id(id) {
      return (id);
   }
   get firstName(firstName) {
      return (firstName);
   }
   get lastName(lastName) {
      return (lastName);
   }
   get birthday(birthday) {
      return (birthday);
   }
   get salary(salary) {
      return (salary);
   }
   set age(age) {
      this.age = age;

   }
   static get EMPLOYEES() {
      return Employee._EMPLOYEES;
   }

   quit() {
      if (!Employee._EMPLOYEES) {
         Employee._EMPLOYEES = [];
      }
      Employee._EMPLOYEES.push(this);
   }
   retire() {

   }
   getFired() {

   }
   changeDepartment(newDepartment) {

   }
   changePosition(newPosition) {

   }
   changeSalary(newSalary) {

   }
   getPromoted(benefits) {

   }
   getDemoted(punishment) {

   }
}

class Manager extends Employee {
   constructor(id, firstName, lastName, birthday, salary, department, age) {
      super(id, firstName, lastName, birthday, salary, 'manager', department, age)
      this.__managedEmployees = [];
   }
}

class BlueCollarWorker extends Employee { }

class HRManager extends Manager { }

class SalesManager extends Manager { }*/